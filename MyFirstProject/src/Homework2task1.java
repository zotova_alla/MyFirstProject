import java.util.ArrayList;
import java.util.*;

public class Homework2task1 {

    public static void main(String[] args) {
        ArrayList<String> states = new ArrayList<String>();
        states.add("Alabama");
        states.add("Alaska");
        states.add("California");
        states.add("Florida");
        states.add("Delaware");
        states.add("Hawaii");
        states.add("Indiana");
        states.add("Massachusetts");
        states.add("Nebraska");
        states.add("New York");

        System.out.println("Original List : " + states);
        states.set(7, "California");
     //   Collections.rotate(states,5);
        System.out.println("Rotated List: " + states);
    }


}
