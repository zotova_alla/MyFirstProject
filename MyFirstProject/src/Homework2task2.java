import java.util.Arrays;

public class Homework2task2 {

    public static void main(String[] args) {
        int[] reverseArray = new int[]{1, 1, 3, 7, 7, 8, 9, 9, 9, 10};
        int reverseArrayWithoutDuplicates = reverseArray[0];
        boolean foundElement = false;
        String reverseArrayToString = Arrays.toString(reverseArray);
        System.out.println("Array: " + reverseArrayToString);

        System.out.print("Reverse Array without duplicates: ");
        for (int i = reverseArray.length - 1; i >= 0; i--) {
            if (reverseArrayWithoutDuplicates == reverseArray[i] && !foundElement) {
                foundElement = true;
                } else if (reverseArrayWithoutDuplicates != reverseArray[i]) {
                    System.out.print(reverseArrayWithoutDuplicates + " ");
                    reverseArrayWithoutDuplicates = reverseArray[i];
                    foundElement = false;
                }
            }
        }
    }