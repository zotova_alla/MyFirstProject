import java.util.Random;
import java.util.Arrays;
public class Homework2task3 {

    public static void main(String[] args) {
        int[] array = new int[10];
        float average_value = 0, sum_of_elements;
        Random rand = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(50);
        }
        System.out.println("Array:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "  ");
        }

        int min_value = array[0], index = 0;
        for (int i = 0; i < array.length; i++) {
            if (min_value >= array[i]) {
                min_value = array[i];
                index = i;
            }
        }
        System.out.println("\n Minimal value = " + min_value);

        int number_of_elements = 10;
        sum_of_elements = 0;
        for (int i = 0; i < number_of_elements; i++) {
            sum_of_elements += array[i];
           // average_value = sum_of_elements / number_of_elements;
        }
        average_value = sum_of_elements / number_of_elements;
        System.out.println("\n Average value = " + average_value);
        array[index] = (int) average_value;

        System.out.println("New array = " + Arrays.toString(array));
    }
}