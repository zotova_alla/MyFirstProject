package homework3task1;

public class Dog implements IPet {

    @Override
    public void callSound() {
        System.out.println("Barking");

    }

    @Override
    public void runSpeed() {
        System.out.println("30 m/sec");

    }
}
