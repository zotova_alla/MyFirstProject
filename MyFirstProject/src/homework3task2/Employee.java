package homework3task2;

public class Employee implements ITeam {
    private String name;
    public Employee(){
    }
    public Employee(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

    @Override
    public void openTask() {
        System.out.println(this.getName() + " open task");
    }

    @Override
    public void reopenTask() {
        System.out.println(this.getName() + " reopen task");
    }

    @Override
    public void resolveTask() {
        System.out.println(this.getName() + " resolve task");
    }
}
