package homework3task2;

public abstract class EmployeeDefault implements ITeam {
    protected Employee employee;
    public EmployeeDefault(Employee employee){
        this.employee = employee;
    }


    @Override
    public void openTask() { employee.openTask(); }

    @Override
    public void reopenTask() {
        employee.reopenTask();
    }

    @Override
    public void resolveTask() {
    employee.resolveTask();
    }
}
