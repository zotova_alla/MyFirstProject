package homework3task2;

public interface ITeam {
    void openTask();
    void reopenTask();
    void resolveTask();

}
