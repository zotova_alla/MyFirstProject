package homework3task2;

public class Main {
    public static void main(String[] args) {
        Employee emp1 = new Employee("John Snow");
        emp1.openTask();
        emp1.reopenTask();
        emp1.resolveTask();
    }
}
