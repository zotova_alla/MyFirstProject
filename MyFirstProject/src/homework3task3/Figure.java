package homework3task3;

public abstract class Figure {
double height;
double length1;
double length2;
double length3;

    public Figure(double a, double b) {
       length1 = a;
       length2 = b;
    }

    public Figure(double c, double d, double e, double f) {
        height = c;
        length1 = d;
        length2 = e;
        length3 = f;
    }


    abstract double area();
    abstract double perimeter();


}
