package homework3task3;

public class Rectangle extends Figure {

    public Rectangle(double a, double b) {
        super(a, b);
    }

    @Override
    double area() {
        return length1*length2;
    }

    @Override
    double perimeter() {
        return 2 * (length1 + length2);
    }
}
