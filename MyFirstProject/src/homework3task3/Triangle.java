package homework3task3;

public class Triangle extends Figure {

    public Triangle(double a, double b, double c, double d) {
        super(a, b, c, d);
    }

    @Override
    double area() {
        return (length1 * height) / 2;
    }

    @Override
    double perimeter() {
        return length1 + length2 + length3;
    }
}
