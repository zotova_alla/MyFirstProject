package homework4exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class ExceptionsSamples {
    private String word;

    public static void nullPointerException(String name) throws NullPointerException
    {
        System.out.println(name.toLowerCase());
    }

    public static void indexOutOfBounds() throws IndexOutOfBoundsException{
        int array[] = {1, 2, 3, 4};
        System.out.println(array[5]);
        }

    public static void numberFormatException() throws NumberFormatException{
        String word = "TEST";
        int i = Integer.parseInt(word);
        System.out.println("int value = " + i);
    }

    public static void classCastException() throws ClassCastException{
        Object object=new Integer(10);
        System.out.println("The value is "+(String)object);
    }

    public static void classNotFoundException() throws ClassNotFoundException{
       // Class.forName("oracle.jdbc.driver.OracleDriver");
    }

    public static void illegalArgumentException() throws IllegalArgumentException{
        int a = 12;
        int b=0;
        int c = a/b;
        System.out.println(c);
    }

   public static void illegalStateException() throws IllegalStateException{
       new ArrayList().listIterator().remove();
   }

   public static void unsupportedOperatedException() throws UnsupportedOperationException{
       String[] strArray = {"Sun", "Mon", "Tue"};
       List list = Arrays.asList(strArray);
       ListIterator listItr = list.listIterator();

       if(listItr.hasNext()){
           listItr.next();
           listItr.set("Sat");
           listItr.add("Wed");
           listItr.remove();
       }
   }

   public static void error(){
       long[] array = new long[Integer.MAX_VALUE];
       System.out.println(array);
   }

    public static void fileNotFound() throws FileNotFoundException {
        Scanner input = new Scanner(new File("file.txt"));
   }

    public static void main(String[] args) {

        try {
            fileNotFound();
            classNotFoundException();
            indexOutOfBounds();
            nullPointerException(null);
            numberFormatException();
            classCastException();
            indexOutOfBounds();
            illegalArgumentException();
            illegalStateException();
            unsupportedOperatedException();
        } catch (Exception e) {
            e.printStackTrace();
        }

        error();
    }

}