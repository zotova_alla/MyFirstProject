package com.lesson4.task1;

public class Cat implements IPet {

    @Override
    public String callSound() {
        System.out.println("Purr");
        return "Purr";
    }

    @Override
    public void runSpeed() {
        System.out.println("12 m/sec");

    }
}
