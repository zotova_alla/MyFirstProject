package com.lesson4.task1;

public class Dog implements IPet {

    @Override
    public String callSound() {
        System.out.println("Barking");

        return "Barking";
    }
    @Override
    public void runSpeed() {
        System.out.println("30 m/sec");

    }
}
