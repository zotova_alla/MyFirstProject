package com.lesson4.task1;

public class Main {

    public static void main(String[] args) {
        IPet cat = new Cat();
        cat.callSound();
        cat.runSpeed();

        IPet dog = new Dog();
        dog.callSound();
        dog.runSpeed();
    }
}
