package com.tests.lesson4.task1;

import com.lesson4.task1.Cat;
import com.lesson4.task1.Dog;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Tests {
    Dog dog = new Dog();
    Cat cat = new Cat();

    @Test
    void dogCallSoundTest(){
        Assert.assertEquals("Barking", dog.callSound()); }

@Test
    void catCallSound(){
        String result = cat.callSound();
        Assert.assertTrue(result.equals("Purr"), result + "That's true");
}

@DataProvider(name = "callSoundTest2")
    private Object [][] setupCallSoundTest2(){
        return new Object[][]{{"Singing"},{"Dancing"},{"Barking"}

        };
}

@Test(dataProvider = "callSoundTest2")
    void setupCallSoundTest2test(String targetStr ) {
        Assert.assertEquals(targetStr, dog.callSound());
}
}
